### Setup ###

Install [Ruby](https://www.ruby-lang.org/en/downloads/) and [Docker](https://www.docker.com/products/overview#/install_the_platform).

Run `docker-compose up -d` and `bundle install`. 

After that, just run the tests with `bundle exec cucumber`.

### View the tests running with VNC: ###

Install [RealVNC Viewer](https://www.realvnc.com/download/viewer/).

Run `docker ps | grep firefox` to know which port firefox is running:

```
9c3c89107863        selenium/node-firefox-debug   "/opt/bin/entry_point"   About an hour ago   Up About an hour   
 0.0.0.0:32773->5900/tcp   askcharliechallenge_firefoxnode_1
```

Connect with `locahost:32773` and type the password `secret`.