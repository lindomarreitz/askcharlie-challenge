require_relative "../pages/home_page.rb"

Given(/^I am in the home page$/) do
  visit "https://askcharlie.de"
end

When(/^I select the service "([^"]*)"$/) do |option|
  @home_page.select_service(option)
end

When(/^I fill the zip code "([^"]*)"$/) do |zip|
  @home_page.fill_zip_code(zip)
end

When(/^I click on submit$/) do
  @home_page.click_on_submit
end

When(/^I select the required fields$/) do
  @request_service_page.click_on_start

  @request_service_page.select_option
  @request_service_page.click_on_next

  @request_service_page.select_option
  @request_service_page.click_on_next

  @request_service_page.select_option
  @request_service_page.click_on_next

  @request_service_page.select_option
  @request_service_page.click_on_next

  @request_service_page.select_option
  @request_service_page.click_on_next

  @request_service_page.select_option
  @request_service_page.click_on_next

  @request_service_page.select_option
  @request_service_page.click_on_next

  @request_service_page.click_on_next
  @request_service_page.click_on_next
end

When(/^I fill the email "([^"]*)"$/) do |email|
  @contact_details_page.fill_email(email)
end

When(/^I click on quotation request$/) do
  @contact_details_page.click_on_quotation_request
end

When(/^I fill the contact details$/) do
  @contact_details_page.select_mr_title

  @contact_details_page.fill_first_name("Lindomar")
  @contact_details_page.fill_last_name("CS_Charlie")
  @contact_details_page.fill_email("lindomar@test.com")
  @contact_details_page.fill_telephone("017665290112")
end

Then(/^I am redirected to contact details page$/) do
  expect(page).to have_current_path("/questionnaire/massage/contact-details")
end

Then(/^I am redirected to home page$/) do
  expect(page).to have_current_path("/")
end

Then(/^I see the message "([^"]*)"$/) do |message|
  expect(@contact_details_page.get_quotation_message).to eq(message)
end

Then(/^I see the confirmation message "([^"]*)"$/) do |message|
  expect(@contact_details_page.get_headline_message).to eq(message)
end

Then(/^I see the error message "([^"]*)"$/) do |message|
  expect(@contact_details_page.get_email_error_message).to eq(message)
end
