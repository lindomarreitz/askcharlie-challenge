class HomePage
  include Capybara::DSL

  def select_service(option)
    page.select option, :from => "forms_request_service_service"
  end

  def fill_zip_code(zip)
    fill_in "forms_request_service_zip", with: zip
  end

  def click_on_submit
    find(".button--no-margin").click
  end
end
