class RequestServicePage
  include Capybara::DSL

  def click_on_start
    find(".request_service__button").click
  end

  def select_option
    first(".ng-scope").click
  end

  def click_on_next
    find(".form-section__button").click
  end
end
