class ContactDetailsPage
  include Capybara::DSL

  def select_mr_title
    find("#title_mr").click
  end

  def fill_first_name(name)
    fill_in "first_name", with: name
  end

  def fill_last_name(name)
    fill_in "last_name", with: name
  end

  def fill_email(email)
    fill_in "customer_email", with: email
  end

  def fill_telephone(telephone)
    fill_in "telephone", with: telephone
  end

  def click_on_quotation_request
    find(".contact-details-page__submit").click
  end

  def get_email_error_message
    find(".error_customer_email").text
  end

  def get_quotation_message
    find(".banner--light-blue").text
  end

  def get_headline_message
    find(".success-page--info").find(".headline").text
  end
end
