require 'capybara/cucumber'
require 'selenium-webdriver'

capabilities = Selenium::WebDriver::Remote::Capabilities.firefox
Capybara.default_driver = :remote_browser

Capybara.register_driver :remote_browser do |app|
  Capybara::Selenium::Driver.new(app, :browser => :remote, :url => "http://localhost:4444/wd/hub", :desired_capabilities => capabilities)
end

Capybara.javascript_driver = :remote_browser
Capybara.run_server = false

Before do
  @home_page = HomePage.new
  @request_service_page = RequestServicePage.new
  @contact_details_page = ContactDetailsPage.new
end
