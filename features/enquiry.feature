Feature: Enquiry

As a user
I should be able to create an enquiry
So I can receive a list of service providers

Scenario: Select a service
Given I am in the home page
When I select the service "Massage"
And I fill the zip code "10557"
And I click on submit
And I select the required fields
Then I am redirected to contact details page
And I see the message "Wir ermitteln nun die besten Masseur und stellen sie Ihnen vor (max. 5)."

Scenario: Create an enquiry
Given I am in the home page
When I select the service "Massage"
And I fill the zip code "10557"
And I click on submit
And I select the required fields
And I fill the contact details
And I click on quotation request
Then I see the confirmation message "Fast geschafft!"

Scenario: Create an enquiry with invalid email
Given I am in the home page
When I select the service "Massage"
And I fill the zip code "10557"
And I click on submit
And I select the required fields
And I fill the email "lindomar@"
And I click on quotation request
Then I see the error message "Bitte geben Sie eine gültige E-Mail-Adresse an"

Scenario Outline: Select a service with a invalid zip code
Given I am in the home page
When I select the service "Massage"
And I fill the zip code <zip_code>
Then I am redirected to home page

Examples:
  | zip_code |
  | "-1"     |
  | "99999"  |
  | ""       |
